from PyQt5 import QtWidgets
from PyQt5.QtWidgets import *
import sys

def demo():
    app = QApplication(sys.argv)
    win = QMainWindow()
    win.setGeometry(300,300,300,300)
    win.setWindowTitle("OpenCV functions GUI")
    label = QtWidgets.QLabel(win)
    label.setText("Hello vole")
    label.move(100,100)
    
    win.show()
    sys.exit(app.exec_())
    
def _createMenuBar(self):
    menuBar = self.menuBar()
    # File menu
    fileMenu = QMenu("&File", self)
    menuBar.addMenu(fileMenu)
    fileMenu.addAction(self.newAction)
    fileMenu.addAction(self.openAction)
    fileMenu.addAction(self.saveAction)
    fileMenu.addAction(self.exitAction)
    # Edit menu
    editMenu = menuBar.addMenu("&Edit")
    editMenu.addAction(self.copyAction)
    editMenu.addAction(self.pasteAction)
    editMenu.addAction(self.cutAction)
    # Help menu
    helpMenu = menuBar.addMenu(QIcon(":help-content.svg"), "&Help")
    helpMenu.addAction(self.helpContentAction)
    helpMenu.addAction(self.aboutAction)
    
    
if __name__ == '__main__':
    demo()