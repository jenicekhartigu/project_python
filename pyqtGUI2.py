
import random
import sys

import math
from tkinter import W
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from PIL import ImageQt
from functionsGui import *

labels = []

btnGrp1 = QButtonGroup()
btnGrp2 = QButtonGroup()

mainLayout = QGridLayout()

class MainWindow(QMainWindow):
    """Main Window."""
    
    
    def __init__(self, parent=None):
        """Initializer."""
        super().__init__(parent)
        
        
        self.setWindowTitle("OpenCV functions GUI")
        
        self.initGUI()
        self.resize(800, 400)
        
        # self.label1 = QLabel()
        for i in range(len(labels)):
            labels[i].setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        
        
        # self.centralWidget = QLabel("Hello world")
        # self.centralWidget.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        # self.setCentralWidget(self.centralWidget)
        
    def clickMethod(self):
        print('Clicked Pyqt button.')
        
    def randColor(self):
        color = QtGui.QColor(*random.sample(range(255), 3))
        return color
        
    def initGUI(self):
        self._createActions()
        self._createMenuBar()
        
        central_widget = QWidget()
        self.setCentralWidget(central_widget)
        
        label1 = QLabel("Photo 1")
        label2 = QLabel('Photo 2')
        label3 = QLabel('Functions 1')
        
        btn1 = QPushButton('Original')
        btn1.clicked.connect(self.showOriginal)
        btn2 = QPushButton('Show histogram')
        btn2.clicked.connect(self.makeHistogram)
        label5 = QLabel('Functions Set 1')
        label6 = QLabel('Functions Set 2')
        label7 = QLabel('Functions Set 3')
        label8 = QLabel('Functions Set 4')
        
        labels.append(label1)
        labels.append(label2)
        labels.append(label3)
        labels.append(label5)
        labels.append(label6)
        labels.append(label7)
        labels.append(label8)
        
        radioBtn1 = QRadioButton("RGB")
        radioBtn2 = QRadioButton("GRAY")
        radioBtn3 = QRadioButton("BGR")

        btnGrp1.addButton(radioBtn1)
        btnGrp1.addButton(radioBtn2)
        btnGrp1.addButton(radioBtn3)
        
        radioBtn4 = QRadioButton("FindRed")
        radioBtn5 = QRadioButton("FindGreen")
        radioBtn6 = QRadioButton("FindBlue")

        btnGrp2.addButton(radioBtn4)
        btnGrp2.addButton(radioBtn5)
        btnGrp2.addButton(radioBtn6)
        
        mainLayout.addWidget(labels[0], 0, 0, 1, 2)
        mainLayout.addWidget(labels[1], 0, 2, 1, 2)
        mainLayout.addWidget(labels[2], 1, 0, 1, 2)
        mainLayout.addWidget(btn1, 1, 2, 1, 1)
        mainLayout.addWidget(btn2, 1, 3, 1, 1)
        # self.mainLayout.addWidget(labels[3], 2, 0, 1, 1)
        mainLayout.addWidget(radioBtn1, 2, 0, 1, 1)
        mainLayout.addWidget(radioBtn2, 3, 0, 1, 1)
        mainLayout.addWidget(radioBtn3, 4, 0, 1, 1)
        #self.mainLayout.addWidget(labels[4], 2, 1, 3, 1)
        mainLayout.addWidget(radioBtn4, 2, 1, 1, 1)
        mainLayout.addWidget(radioBtn5, 3, 1, 1, 1)
        mainLayout.addWidget(radioBtn6, 4, 1, 1, 1)
        
        mainLayout.addWidget(labels[5], 2, 2, 1, 1)
        mainLayout.addWidget(labels[6], 2, 3, 1, 1)
        
        for i in range(len(labels)):
            labels[i].setStyleSheet("background-color: {}".format(self.randColor().name()))
        
        
        mainLayout.setRowStretch(0,3)
        mainLayout.setRowStretch(1,2)
        mainLayout.setRowStretch(2,2)

        central_widget.setLayout(mainLayout) # <---
        self.setLayout(mainLayout)
        self.setMinimumSize(1400, 800)
        # self.showMaximized()
        

        
    

        
    def _createMenuBar(self):
        menuBar = self.menuBar()
        # File menu
        fileMenu = QMenu("&File", self)
        menuBar.addMenu(fileMenu)
        fileMenu.addAction(self.openAction)
        fileMenu.addAction(self.saveAction)
        fileMenu.addAction(self.saveAsAction)
        fileMenu.addAction(self.exitAction)
        
        
        # Edit menu
        erosionsMenu = menuBar.addMenu("&Diletations")
        erosionsMenu.addAction(self.cvErosionSet)
        erosionsMenu.addAction(self.cvDiletationSet)
        erosionsMenu.addAction(self.cvTophatSet)
        erosionsMenu.addAction(self.cvBlackhatSet)
        # Transpoztiton
        transpozitionMenu = menuBar.addMenu("&Transpozitions")
        transpozitionMenu.addAction(self.cvTranspozitionSet)
        # Find color
        findColorMenu = menuBar.addMenu("&Color filter")
        findColorMenu.addAction(self.cvFindColorRed)
        findColorMenu.addAction(self.cvFindColorGreen)
        findColorMenu.addAction(self.cvFindColorBlue)
        findColorMenu.addAction(self.cvFindColorBlack)
        findColorMenu.addAction(self.cvFindColorSpecify)
        # Remove noise 
        removeNoiseMenu = menuBar.addMenu("&Remove noise")
        removeNoiseMenu.addAction(self.cvRemoveNoisePreSet)
        removeNoiseMenu.addAction(self.cvRemoveNoiseSet)
        # Find edges
        findEdgesMenu = menuBar.addMenu("&Find edges")
        findEdgesMenu.addAction(self.cvFindEdgesPreSet)
        findEdgesMenu.addAction(self.cvFindEdgesSet)
        
        
        # Help menu
        helpMenu = menuBar.addMenu("&Help")
        helpMenu.triggered.connect(self.printTest)
        # helpMenu.addAction(self.helpContentAction)
        # helpMenu.addAction(self.aboutAction)
        
    def _createActions(self):
        # File Menu
        ## Open file
        self.openAction = QAction("&Open image", self)
        self.openAction.triggered.connect(self.openImage) 
        ## Save file
        self.saveAction = QAction("&Save image", self)
        ## Save file as
        self.saveAsAction = QAction("&Save image as", self)
        self.saveAsAction.triggered.connect(self.saveAsImage)
        
        self.exitAction = QAction("&Exit", self)
        
        # Diletations Set
        self.cvErosionSet = QAction("&Erosion", self)
        self.cvErosionSet.triggered.connect(self.erosion)
        
        self.cvDiletationSet = QAction("&Diletation", self)
        self.cvDiletationSet.triggered.connect(self.dilate)
        
        self.cvTophatSet = QAction("&Tophat", self)
        self.cvTophatSet.triggered.connect(self.tophat)
        
        self.cvBlackhatSet = QAction("&Blackhat", self)
        self.cvBlackhatSet.triggered.connect(self.blackhat)
        
        # Transpozitions Set
        self.cvTranspozitionSet = QAction("&Transpozitons set")
        
        # Find color set
        self.cvFindColorRed = QAction("&Find red")
        self.cvFindColorRed.triggered.connect(self.findRedPart)
        
        self.cvFindColorGreen = QAction("&Find green")
        self.cvFindColorGreen.triggered.connect(self.findGreenPart)
        
        self.cvFindColorBlue = QAction("&Find blue")
        
        
        self.cvFindColorBlack = QAction("&Find black")
        self.cvFindColorBlack.triggered.connect(self.findBlackPart)
        
        self.cvFindColorSpecify = QAction("&Set Color")
        
        
        
        # Remove noise
        self.cvRemoveNoisePreSet = QAction("&Remove noise")
        self.cvRemoveNoiseSet = QAction("&Set remove noise")
        # Find edges
        self.cvFindEdgesPreSet = QAction("&Find edges")
        self.cvFindEdgesSet = QAction("&Set find edges")
        
        
        self.helpContentAction = QAction("&Help Content", self)
        self.aboutAction = QAction("&About", self)

       
    imageDict = {}
    
    def printTest(self):
        print("Help")
    
    def showOriginal(self):
        if "result" in self.imageDict:
            self.drawImage(self.imageDict["result"])
        else:
            labels[1].setText("Picture is not loaded")
    
    def makeHistogram(self):
        if "hist" not in self.imageDict:
            pixmap = labels[1].pixmap()
            if pixmap is not None:
                image = ImageQt.fromqpixmap(labels[1].pixmap())
                
                res = self.imgToPixmap(makeHist(image))
                self.imageDict['hist'] = res
                self.drawImage(res)
            else:
                labels[0].setText("Picture is not loaded")
        else:
            self.drawImage(self.imageDict["hist"])
        print("show hist")
        
    
    def drawImage(self ,picture):
        labels[1].setPixmap(picture)   
    
    def imgToPixmap(self, image):
        im2 = image.convert("RGBA")
        data = im2.tobytes("raw", "BGRA")
        qim = QtGui.QImage(data, image.width, image.height, QtGui.QImage.Format_ARGB32)
        pixmap = QtGui.QPixmap.fromImage(qim)
        return pixmap
    
    # Diletations
    def erosion(self):
        pixmap = labels[0].pixmap()
        if pixmap is not None:
            image = ImageQt.fromqpixmap(labels[0].pixmap())
            res = self.imgToPixmap(erosion(image))
            self.drawImage(res)
        else:
            labels[0].setText("Picture is not loaded")
            
    def dilate(self):
        pixmap = labels[0].pixmap()
        if pixmap is not None:
            image = ImageQt.fromqpixmap(labels[0].pixmap())
            res = self.imgToPixmap(dilatation(image))
            self.drawImage(res)
        else:
            labels[0].setText("Picture is not loaded")
            
    def tophat(self):
        pixmap = labels[0].pixmap()
        if pixmap is not None:
            image = ImageQt.fromqpixmap(labels[0].pixmap())
            res = self.imgToPixmap(tophat(image))
            self.drawImage(res)
        else:
            labels[0].setText("Picture is not loaded")
            
    def blackhat(self):
        pixmap = labels[0].pixmap()
        if pixmap is not None:
            image = ImageQt.fromqpixmap(labels[0].pixmap())
            res = self.imgToPixmap(blackhat(image))
            self.drawImage(res)
        else:
            labels[0].setText("Picture is not loaded")
    
    # Transpozitions
    def drawImage(self ,picture):
        labels[1].setPixmap(picture)
    
    # Find color
    def findRedPart(self):
        pixmap = labels[0].pixmap()
        if pixmap is not None:
            image = ImageQt.fromqpixmap(labels[0].pixmap())
            
            res = self.imgToPixmap(read_jpg_to_red(image))
            self.imageDict['result'] = res
            self.drawImage(res)
        else:
            labels[0].setText("Picture is not loaded")
        
    def findGreenPart(self):
        pixmap = labels[0].pixmap()
        if pixmap is not None:
            image = ImageQt.fromqpixmap(labels[0].pixmap())
            
            res = self.imgToPixmap(read_jpg_to_green(image))
            self.imageDict['result'] = res
            self.drawImage(res)
        else:
            labels[0].setText("Picture is not loaded")
        
    def findBlackPart(self):
        pixmap = labels[0].pixmap()
        if pixmap is not None:
            image = ImageQt.fromqpixmap(labels[0].pixmap())
            
            res = self.imgToPixmap(read_jpg_to_black(image))
            self.imageDict['result'] = res
            self.drawImage(res)
        else:
            labels[0].setText("Picture is not loaded")
        
    def openImage(self):
        imagePath, _ = QFileDialog.getOpenFileName()
        pixmap = QPixmap(imagePath)
        
        labelWidth = labels[0].width()
        labelHeight = labels[0].height()
        
        pixmapWidth = pixmap.width()
        pixmapHeight = pixmap.height()
        
        pixmapRatio = pixmapHeight/pixmapWidth
        
        newHeight = int(pixmapRatio*labelWidth)
        newWidth = int(pixmapRatio*labelHeight)
        
        if (pixmapWidth > pixmapHeight):
            pixmap1 = pixmap.scaled(labelWidth,newHeight,Qt.KeepAspectRatio, Qt.FastTransformation)
        else:
            pixmap1 = pixmap.scaled(newWidth,labelHeight,Qt.KeepAspectRatio, Qt.FastTransformation)
        # pixmap1 = pixmap.scaled(labelWidth,newHeight,Qt.KeepAspectRatio, Qt.FastTransformation)
        # pixmap1 = pixmap.scaled(newWidth,labelHeight,Qt.KeepAspectRatio, Qt.FastTransformation)
        # pixmap2 = pixmap.scaled(labelWidth,newHeight,Qt.KeepAspectRatio, Qt.FastTransformation)
        labels[0].setPixmap(pixmap1)
        # labels[1].setPixmap(pixmap2)
    
        
        
        
        return imagePath
        
        
    def saveAsImage(self):
        print("henlo")
        pixmap = labels[1].pixmap()
        if pixmap is not None:
            filename, _ = QFileDialog.getSaveFileName(self)
            pixmap = labels[1].pixmap()
            if pixmap is not None and filename:
                pixmap.save(filename)
        else:
            labels[1].setText("Picture is not loaded")
            
    
            

def main():
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()